var spawn = require('child_process').spawn;
var async = require('async');
var tmp = require('tmp');
var fs = require('fs');
var path = require('path');
var rimraf = require('rimraf');
var UglifyJS = require("uglify-es");
var log = require('winston');
var ncp = require("ncp").ncp;

module.exports = function(options, cb)
{
	log.info('Packing browser App To: %s', options.outputDir);

	var packageFile = path.resolve(options.packageFilepath);

	//return if already exists.
	if(fs.existsSync(packageFile)){
		log.info('Package already exists: ', packageFile);
		return cb(null, createResult(options));
	}

	async.waterfall([
		async.apply(runNpmBuild, options),
		copyAssetstoTempDir,
		updateJsFilesToTempDir,
		zipDirectory
	], function(){
		return cb(null, createResult(options));
	});
}

function createResult(options){
	return  [
		{
			file: path.resolve(options.packageFilepath),
			metadata:
			{
				name: options.project,
				version: options.version,
				extension: '.zip',
				packageType: 'webpack-app',
				deployment: 'server'
			}
		}
	];
}

function runNpmBuild(options, cb){

	var npm_build = spawn('npm.cmd', ['run', 'build']);
	var hasError;

	npm_build.stdout.on('data', (data) => {
	  log.debug(data.toString());
	});

	npm_build.stderr.on('data', (data) => {
		hasError = true;
	  log.error(data.toString());
	});

	npm_build.on('close', (code) => {

		if(hasError) return callback('An error occured during the npm run build command.');

		log.info('browser package completed');

		return cb(null, options);
	});
}

function copyAssetstoTempDir(options, cb) {
	var tempPackageDir = '../tmp';
	if (!fs.existsSync(tempPackageDir)){
        fs.mkdirSync(tempPackageDir);
    }

	log.info("Copying to temporary folder");

	ncp('./bin', tempPackageDir,(err) => {
		if(err) {
			cb(err)
		}
		
		cb(null, options);
	})
}

function updateJsFilesToTempDir(options,cb)
{
	log.info('Uglifying javascript')
	var packageJsFileName = path.join('./bin', [options.project.replace('.','-').toLowerCase() , '.js'].join(''));
	var publishJsFileName = path.join('../tmp', [options.project.replace('.','-').toLowerCase() , '.js'].join(''));
	var mapFileName = [options.project.replace('.','-').toLowerCase() , '.js'].join('');

	if(fs.existsSync(packageJsFileName))
	{
		var mapExists = fs.existsSync(packageJsFileName + '.map');

		var result = UglifyJS.minify(fs.readFileSync(packageJsFileName, "utf8"), {
			sourceMap: {
				filename: mapFileName,
				url: mapFileName + '.map'
			}
		});

		fs.truncate(publishJsFileName, 0, function() {
			fs.writeFileSync(publishJsFileName, result.code);
		});

		fs.truncate(publishJsFileName, 0, function() {
			fs.writeFileSync(publishJsFileName + ".map", result.map);
		});
	}

	cb(null, options);
}

function zipDirectory(options, cb){
	var outputFilename = options.packageFilepath;
	log.info("Packing to %s", outputFilename);
	if(fs.existsSync(outputFilename)) fs.unlinkSync(outputFilename);

	options.fn.zipHelper.zipDir('../tmp', outputFilename, function(e, r){ cb(e, options) });
}
